package com.ispy.reportingapi.controller.about;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AboutController {

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about(){

        return "This reporting API is developed by Ijaaz Lagardien in conjunction with" +
                "Drakenstein Municipality and Cape Peninsula University or Technology (CPUT).\n" +
                "For any suggestions email 214167542@mycput.ac.za or contact Drakenstein Municipality directly using the app\n" +
                "© 2021 Ijaaz Lagardien. All Rights Reserved";
    }
}

package com.ispy.reportingapi.controller.survey;

import com.ispy.reportingapi.domain.survey.Survey;
import com.ispy.reportingapi.repository.survey.SurveyRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/survey")
public class SurveyController {

    private final SurveyRepository repository;

    public SurveyController(SurveyRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/private/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Survey> findAll(){
        return repository.findAll();
    }


    @RequestMapping(value = "/public/save/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public Survey save(@RequestBody Survey survey){
        return repository.save(survey);
    }


}

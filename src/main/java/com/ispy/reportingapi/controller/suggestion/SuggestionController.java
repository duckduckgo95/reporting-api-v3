package com.ispy.reportingapi.controller.suggestion;

import com.ispy.reportingapi.domain.suggestion.Suggestion;
import com.ispy.reportingapi.exceptions.complaint.ComplaintNotDeletedException;
import com.ispy.reportingapi.exceptions.suggestion.SuggestionNotFoundException;
import com.ispy.reportingapi.repository.suggestion.SuggestionRepository;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = "/suggestion")
public class SuggestionController {


    private final SuggestionRepository repository;

    public SuggestionController(SuggestionRepository repository) {
        this.repository = repository;
    }


    @RequestMapping(value = "/private/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Suggestion> findAllSuggestions(){
        return repository.findAll();
    }

    @RequestMapping(value = "/private/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Suggestion findById(@PathVariable Long id){
        return repository.findById(id).orElseThrow(() -> new SuggestionNotFoundException(id));
    }

    @RequestMapping(value = "/public/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    public Suggestion save(@RequestBody Suggestion suggestion){
        return repository.save(suggestion);
    }

    @RequestMapping(value = "/public/update/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    public Suggestion update(@PathVariable Long id, @RequestBody Suggestion suggestion){
        return repository.findById(id)
                .map(persistedSuggestion ->{
                    persistedSuggestion.setSuggestion(suggestion.getSuggestion());
                    return repository.save(persistedSuggestion);
                }).orElseGet(() ->{
                    suggestion.setId(id);
                    return repository.save(suggestion);
                });
    }

    @RequestMapping(value = "/private/implemented/{id}", method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Suggestion implemented(@PathVariable Long id){
        return repository.findById(id)
                .map(persistedSuggestion -> {
                   persistedSuggestion.setDateImplemented(new Date());
                   persistedSuggestion.setImplemented(Boolean.TRUE);
                   return repository.save(persistedSuggestion);
                }).orElseThrow(()
                    -> new SuggestionNotFoundException(id)
                );
    }

    @RequestMapping(value = "/private/delete/{id}",method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id){
        if(repository.existsById(id)){
            repository.deleteById(id);
        }else{
            throw new ComplaintNotDeletedException(id);
        }
    }


}

package com.ispy.reportingapi.controller.complaint;

import com.ispy.reportingapi.domain.Complaint.Complaint;
import com.ispy.reportingapi.exceptions.complaint.ComplaintNotDeletedException;
import com.ispy.reportingapi.exceptions.complaint.ComplaintNotFoundException;
import com.ispy.reportingapi.repository.Complaint.ComplaintRepository;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = "/reporting")
public class ComplaintController {

    private final ComplaintRepository repository;

    public ComplaintController(ComplaintRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/private/" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Complaint> findAllComplaints(){
        return repository.findAll();
    }


    @RequestMapping(value = "/private/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Complaint findBYId(@PathVariable Long id){
        return repository.findById(id).orElseThrow(() -> new ComplaintNotFoundException(id));
    }

    @RequestMapping(value = "public/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Complaint save(@RequestBody Complaint complaint){
        return repository.save(complaint);
    }

    @RequestMapping(value = "public/update/{id}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Complaint update(@PathVariable Long id, @RequestBody Complaint updatedComplaint){
        return repository.findById(id)
                .map(persistedComplaint -> {
                    persistedComplaint.setFirstName(updatedComplaint.getFirstName());
                    persistedComplaint.setLastName(updatedComplaint.getLastName());
                    persistedComplaint.setCellNumber(updatedComplaint.getCellNumber());
                    persistedComplaint.setAddress(updatedComplaint.getAddress());
                    persistedComplaint.setLatitude(updatedComplaint.getLatitude());
                    persistedComplaint.setLongitude(updatedComplaint.getLongitude());
                    persistedComplaint.setDescription(updatedComplaint.getDescription());
                    persistedComplaint.setDateOfComplaint(updatedComplaint.getDateOfComplaint());
                    persistedComplaint.setResolved(updatedComplaint.getResolved());
                    persistedComplaint.setDepartment(updatedComplaint.getDepartment());
                    return repository.save(persistedComplaint);
                }).orElseGet(() ->{
                    updatedComplaint.setId(id);
                    return repository.save(updatedComplaint);
                });
    }

    @RequestMapping(value = "private/resolve/{id}", method = RequestMethod.PATCH,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Complaint resolve(@PathVariable Long id){
        return repository.findById(id)
                .map(persistedComplaint ->{
                    persistedComplaint.setResolved(Boolean.TRUE);
                    persistedComplaint.setDateResolved(new Date());
                    return repository.save(persistedComplaint);
                }).orElseThrow(()
                        -> new ComplaintNotFoundException(id)
                );
    }

    @RequestMapping(value = "private/delete/{id}", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id){
        if(repository.existsById(id)){
            repository.deleteById(id);
        }else{
            throw new ComplaintNotDeletedException(id);
        }
    }
}

package com.ispy.reportingapi.domain.Complaint;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Complaint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String cellNumber;
    @Column(length = 500)
    private String address;
    private String latitude;
    private String longitude;
    @Column(length = 500)
    private String description;
    private Date dateOfComplaint;
    private String department;
    private Boolean resolved;
    private Date dateResolved;

}


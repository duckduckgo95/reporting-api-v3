package com.ispy.reportingapi.domain.survey;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 500)
    private String answer1;
    @Column(length = 500)
    private String answer2;
    @Column(length = 500)
    private String answer3;
    @Column(length = 500)
    private String answer4;

}


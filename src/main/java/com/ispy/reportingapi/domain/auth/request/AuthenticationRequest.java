package com.ispy.reportingapi.domain.auth.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthenticationRequest {

    String username;
    String password;
}

package com.ispy.reportingapi.exceptions.suggestion;

public class SuggestionNotFoundException extends RuntimeException{
    public SuggestionNotFoundException(Long id) {
        super("Could not find the suggestion with ID: " + id);
    }
}

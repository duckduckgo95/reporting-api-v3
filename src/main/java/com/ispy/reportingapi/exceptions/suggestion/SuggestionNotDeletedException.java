package com.ispy.reportingapi.exceptions.suggestion;

public class SuggestionNotDeletedException extends RuntimeException{

    public SuggestionNotDeletedException(Long id) {
        super("Could not delete the suggestion with ID: " + id);
    }
}

package com.ispy.reportingapi.exceptions.suggestion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class SuggestionExceptionAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(SuggestionNotFoundException.class)
    public String suggestionNotFoundHandler(SuggestionNotFoundException exception){
        return exception.getMessage();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ExceptionHandler(SuggestionNotDeletedException.class)
    public String suggestionNotDeletedHandler(SuggestionNotDeletedException exception){
        return exception.getMessage();
    }
}

package com.ispy.reportingapi.exceptions.complaint;

public class ComplaintNotFoundException extends RuntimeException {

    public ComplaintNotFoundException(Long id) {
        super("Could not find the complaint you are looking for with id: " + id);
    }
}

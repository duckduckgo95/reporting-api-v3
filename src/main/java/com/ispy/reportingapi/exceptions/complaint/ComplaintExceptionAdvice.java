package com.ispy.reportingapi.exceptions.complaint;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class ComplaintExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(ComplaintNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String complaintNotFoundHandler(ComplaintNotFoundException exception){
        return exception.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(ComplaintNotDeletedException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String complaintNotDeletedHandler(ComplaintNotDeletedException exception){
        return exception.getMessage();
    }

}

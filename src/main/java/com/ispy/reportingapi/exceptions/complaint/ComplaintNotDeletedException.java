package com.ispy.reportingapi.exceptions.complaint;

public class ComplaintNotDeletedException extends RuntimeException{

    public ComplaintNotDeletedException(Long id) {
        super("Could not delete the complaint with id: " + id);
    }
}

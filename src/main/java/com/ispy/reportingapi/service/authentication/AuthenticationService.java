package com.ispy.reportingapi.service.authentication;

import com.ispy.reportingapi.domain.user.Account;
import com.ispy.reportingapi.repository.user.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class AuthenticationService implements UserDetailsService {

    private final AccountRepository accountRepository;

    public AuthenticationService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Account account =accountRepository.findOneByUsername(username);

        if(account == null){
            throw new UsernameNotFoundException("Username: " + username + " not found");
        }

        return createUser(account);
    }

    private User createUser(Account account){
        return new User(account.getUsername(),account.getPassword(), getGrantedAuthorities(account));
    }

    private Collection<GrantedAuthority> getGrantedAuthorities(Account account){
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        if(account.getRole().getName().equalsIgnoreCase("admin")){
            authorities.add(new SimpleGrantedAuthority("ROLE_" + account.getRole().getName()));
        }

        return authorities;
    }


}

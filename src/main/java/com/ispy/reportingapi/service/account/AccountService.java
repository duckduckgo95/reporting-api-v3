package com.ispy.reportingapi.service.account;

import com.ispy.reportingapi.domain.role.AccountRole;
import com.ispy.reportingapi.domain.user.Account;
import com.ispy.reportingapi.repository.role.AccountRoleRepository;
import com.ispy.reportingapi.repository.user.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountRoleRepository accountRoleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountService(AccountRepository accountRepository, AccountRoleRepository accountRoleRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.accountRoleRepository = accountRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void initialise(){
        if(accountRoleRepository.findByName("ADMIN") == null){
            saveAccountRole(new AccountRole());
        }
        if(accountRepository.findOneByUsername("ijaaz") == null){
            saveAccount(new Account("ijaaz", "ijaaz", accountRoleRepository.findByName("admin")));
        }
        if(accountRepository.findOneByUsername("zahraa") == null){
            saveAccount(new Account("zahraa", "zahraa", accountRoleRepository.findByName("admin")));
        }
    }

    @Transactional
    void saveAccount(Account user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        accountRepository.save(user);

    }

    @Transactional
    void saveAccountRole(AccountRole role){
        role.setName("ADMIN");
        accountRoleRepository.save(role);
    }
}

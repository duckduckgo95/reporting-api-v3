package com.ispy.reportingapi.repository.role;

import com.ispy.reportingapi.domain.role.AccountRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRoleRepository extends JpaRepository<AccountRole, Long> {

    AccountRole findByName(String name);
}

package com.ispy.reportingapi.repository.suggestion;

import com.ispy.reportingapi.domain.suggestion.Suggestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuggestionRepository extends JpaRepository<Suggestion, Long> {
}

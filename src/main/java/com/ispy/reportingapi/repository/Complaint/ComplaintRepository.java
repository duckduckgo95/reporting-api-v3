package com.ispy.reportingapi.repository.Complaint;

import com.ispy.reportingapi.domain.Complaint.Complaint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComplaintRepository extends JpaRepository<Complaint, Long> {
}

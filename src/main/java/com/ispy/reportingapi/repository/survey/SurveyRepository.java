package com.ispy.reportingapi.repository.survey;

import com.ispy.reportingapi.domain.survey.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SurveyRepository extends JpaRepository<Survey, Long> {
}

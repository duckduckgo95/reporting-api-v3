package com.ispy.reportingapi.repository.user;

import com.ispy.reportingapi.domain.user.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findOneByUsername(String username);
}
